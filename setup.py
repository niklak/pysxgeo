import os
from setuptools import setup, find_packages
from pysxgeo import __version__, __author__, __email__

with open(os.path.join(os.path.dirname(__file__),
                       'README.rst')) as f:
    l_d = f.read().strip()

setup(
    name='pysxgeo',
    version=__version__,
    author=__author__,
    author_email=__email__,
    keywords='geolocation',
    description='This API provides access to Sypex Geo 2.2 databases. '
                'The databases available from https://sypexgeo.net/ ',
    long_description=l_d,
    packages=find_packages(exclude=['tests', 'db']),
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    zip_safe=False,
    url='https://bitbucket.org/niklak/pysxgeo/',

)
