import unittest

from pysxgeo.sxgeo import *
from db import get_db

# DB 2015.04.23
db = get_db()
ips = ('77.236.97.247', '61.240.144.64', '91.212.124.160')
ips_info = (
    {'city': {'name_en': 'Sonsbeck', 'lon': 6.36667,
          'name_ru': 'Sonsbeck', 'id': 2831105, 'lat': 51.61667},
     'country': {'iso': 'DE', 'id': 56}},
    {'city': {'name_en': 'Beijing', 'lon': 116.39723,
              'name_ru': 'Пекин', 'id': 1816670, 'lat': 39.9075},
     'country': {'iso': 'CN', 'id': 48}},
    {'city': {'name_en': 'Dnipropetrovsk', 'lon': 34.98333,
              'name_ru': 'Днепропетровск', 'id': 709930, 'lat': 48.45},
     'country': {'iso': 'UA', 'id': 222}}
)
ip_full = {'country': {'id': 56, 'lon': 10.5, 'lat': 51.5,
                       'name_en': 'Germany', 'iso': 'DE', 'name_ru': 'Германия'},
           'region': {'id': 2861876, 'iso': 'DE-NW',
                      'name_en': 'Land Nordrhein-Westfalen',
                      'name_ru': 'Северный Рейн-Вестфалия'},
           'city': {'id': 2831105, 'lon': 6.36667, 'name_ru': 'Sonsbeck',
                    'name_en': 'Sonsbeck', 'lat': 51.61667}}



class FileModeTest(unittest.TestCase):

    def setUp(self):
        self.obj = SxGeo(db)

    def test_header_info(self):
        info = self.obj.info
        for field in opts.INFO_FIELDS:
            self.assertIn(field, info)

    def test_localhost(self):
        local_ips = ('10.0.0.0', '127.0.0.0', '0.0.0.0')
        for ip in local_ips:
            self.assertEqual(self.obj.get(ip), False)

    def test_output(self):
        for i in range(len(ips)):
            self.assertDictEqual(self.obj.get(ips[i]), ips_info[i])

    def test_output_full(self):
        self.assertDictEqual(self.obj.get_city(ips[0], full=True), ip_full)

    def test_coords(self):
        coords = self.obj.get_coords(ips[0])
        chk_coords = 51.61667, 6.36667
        self.assertSequenceEqual(coords, chk_coords)


class MemoryModeTest(FileModeTest):
    def setUp(self):
        self.obj = SxGeo(db, mode=SXGEO_MEMORY)


class BatchModeTest(FileModeTest):
    def setUp(self):
        self.obj = SxGeo(db, mode=SXGEO_BATCH)


if __name__ == '__main__':
    unittest.main()
