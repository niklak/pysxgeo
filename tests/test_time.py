import time
from pysxgeo.sxgeo import *
from db import get_db

ip = '52.0.183.139'
db = get_db()

def show_time(f):
    def wrap(*args, **kwargs):
        start = time.time()
        func = f(*args, **kwargs)
        print('Finished at {0:f}'.format(time.time() - start))
        return func
    return wrap

@show_time
def get_data(mode=SXGEO_FILE):
    geo = SxGeo(db, mode)
    data = geo.get_city(ip)
    del geo
    return data

get_data()
get_data(SXGEO_MEMORY)
get_data(SXGEO_BATCH)
