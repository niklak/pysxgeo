import time
from pysxgeo.sxgeo import *
from db import get_db

ip = '52.0.183.139'
db = get_db()

def best_time(mode):
    obj = SxGeo(db, mode)
    time_tuple = tuple()
    for i in range(1000):
        # Time after creating an object
        t = time.time()
        obj.get_city(ip)
        time_tuple += (time.time() - t,)
    print('Best time is {0:f}'.format(min(time_tuple)))

def best_one_time(mode):
    time_tuple = tuple()
    for i in range(1000):
        # Time before creating an object
        t = time.time()
        obj = SxGeo(db, mode)
        obj.get_city(ip)
        del obj
        time_tuple += (time.time() - t,)
    print('Best time is {0:f}'.format(min(time_tuple)))

best_time(SXGEO_FILE)
best_time(SXGEO_MEMORY)
best_time(SXGEO_BATCH)

best_one_time(SXGEO_FILE)
best_one_time(SXGEO_MEMORY)
best_one_time(SXGEO_BATCH)